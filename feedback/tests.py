from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .models import Feedback
from .forms import FormFeedback
from .views import feedback, landing


class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.new_user = User.objects.create(username="test", email="test@gmail.com", password='helloworld123')
        self.feedback1 = Feedback.objects.create(user=self.new_user, feedback='tingkatkan')
        self.feedback = reverse("feedback:feedback")
        self.landing = reverse("feedback:landing")

    def test_GET_feedback(self):
        response = self.client.get(self.feedback)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "feedback.html")

    def test_GET_landing(self):
        response = self.client.get(self.landing, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_anonymous_confirmation_url(self):
        response = self.client.get('/feedback/landing/')
        self.assertRedirects(response, "/login/?next=/feedback/landing/")

    def test_GET_user_confirmation_url(self):
        self.client.force_login(user=self.new_user)
        response = self.client.get('/feedback/landing/')
        self.assertRedirects(response, "/feedback/")

    def test_GET_all_feedback(self):
        response = self.client.get('/feedback/all_feedback/')
        self.assertEqual(response.status_code, 200)
