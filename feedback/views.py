from django.shortcuts import render, redirect, get_object_or_404
from .forms import FormFeedback
from .models import Feedback
from django.http import Http404
from django.http import HttpResponseRedirect, JsonResponse
from django.contrib.auth.decorators import login_required
from django.forms import inlineformset_factory, modelformset_factory
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.core.serializers import serialize 

# Create your views here.
def feedback(request):
    form = FormFeedback()
    context = {
        'form': form,
    }
    return render(request, 'feedback.html', context)

def create(request):
    if request.method == 'POST':
        feedback = request.POST['feedback']
        # user_name = request.POST['user_name']
        response_data = dict()
        post = Feedback(user=request.user, feedback=feedback)
        post.save()
        return redirect('feedback:feedback')

def feedback_list(request):
    feedback= Feedback.objects.all()
    result = []
    for i in feedback:
        user = User.objects.get(id=i.user.id)
        temp = dict()
        temp['feedback'] = i.feedback
        temp['username'] = user.username
        result.append(temp)
    context = {
        'result': result,
    }
    return JsonResponse(context)

@login_required(login_url=('user:login'))
def landing(request):
    if request.method == "POST":
        form = FormFeedback(request.POST)
        context = {
            'form': form,
            'feedback': request.POST['feedback'],
            # 'user_name': request.POST['user_name'],
        }
        return render(request, 'landing.html', context)
    return redirect('feedback:feedback')
