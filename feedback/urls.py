from django.contrib import admin
from django.conf.urls import url
from django.urls import include, path
from . import views
app_name = 'feedback'

urlpatterns = [
    path('', views.feedback, name='feedback'),
    path('all_feedback/', views.feedback_list, name='all_feedback'),
    path('create/', views.create, name='create'),
    path('landing/', views.landing, name='landing'),
]