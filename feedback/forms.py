from django import forms

#import model dari models.py
from .models import Feedback

class FormFeedback(forms.ModelForm):
    class Meta:
        model = Feedback
        fields = [
            'feedback',
        ]

        labels = {
            'feedback' : 'Kritik/Saran',
        }

        widgets = {

            'feedback' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'id'    : 'feedback',
                }
            ),
        }