from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import inlineformset_factory, modelformset_factory
from django.http.response import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from .forms import RelawanForm
from .models import Relawan
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.core.serializers import serialize 

# Create your views here.
def about(request):
    return render(request, 'About.html')

def info(request):
    form = RelawanForm()
    context = {
        'form': form,
    }
    return render(request, 'info.html', context)

def buat(request):
    if request.method == 'POST':
        relawan = request.POST['relawan']
        response_data = dict()
        post = Relawan(user=request.user, relawan=relawan)
        post.save()
        return redirect('relawan:relawan')
    

def volunteer_list(request):
    relawan = Relawan.objects.all()
    result = []
    for i in relawan:
        user = User.objects.get(id=i.user.id)
        temp = dict()
        temp['relawan'] = i.relawan
        temp['username'] = user.username
        result.append(temp)
    context = {
        'result': result,
    }
    return JsonResponse(context)

@login_required(login_url=('user:login'))
def bisa(request):
    if request.method == "POST":
        form = RelawanForm(request.POST)
        context = {
            'form': form,
            'relawan': request.POST['relawan'],
        }
        return render(request, 'bisa.html', context)
    return redirect('relawan:relawan')
