from django import forms

#import model dari models.py
from .models import Relawan

class RelawanForm(forms.ModelForm):
    class Meta:
        model = Relawan
        fields = [
            'relawan',
        ]

        labels = {
            'relawan' : 'Tujuan/Kontak',
        }

        widgets = {

            'relawan' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'id'    : 'relawan',
                }
            ),
        }