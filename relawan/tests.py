from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from relawan.models import Relawan
from relawan.forms import RelawanForm
from relawan.views import about, info, volunteer_list, bisa, buat
from relawan.apps import RelawanConfig


class ViewsTest(TestCase):
    def setUp(self):
        self.client = Client()
        self.new_user = User.objects.create(username="test", email="hello@gmail.com", password='GWPJE0PWAJWG0WGJ')
        self.relawan = Relawan.objects.create(user=self.new_user, relawan='Volunteer/123456789')
        self.info = reverse("relawan:relawan")
        self.bisa = reverse("relawan:bisa")
        self.about = reverse("relawan:About")

    def test_GET_info(self):
        response = self.client.get(self.info)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "info.html")

    def test_GET_bisa(self):
        response = self.client.get(self.bisa, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_about(self):
        response = self.client.get(self.about, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "About.html")

    def test_GET_anonymous_confirmation_url(self):
        response = self.client.get('/relawan/bisa/')
        self.assertRedirects(response, "/login/?next=/relawan/bisa/")

    def test_GET_user_confirmation_url(self):
        self.client.force_login(user=self.new_user)
        response = self.client.get('/relawan/bisa/')
        self.assertRedirects(response, "/relawan/")

    def test_GET_message_all_data(self):
        response = self.client.get('/relawan/all_volunteer/')
        self.assertEqual(response.status_code, 200)

    def test_apps(self):
        self.assertEqual(RelawanConfig.name, 'relawan') 
