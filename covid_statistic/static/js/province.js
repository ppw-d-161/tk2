$(document).ready(function () {
    var province_data;
    var date;
    $.ajax({
        url: "/statistic/statistic_prov_data/",
        success: function (response) {
            province_data = response.data_covid;
            date = response.last_updated;
            $("#date").append(date);
            var count = 0;
            for (let index = 0; index < province_data.length; index++) {
                var html = "";
                html += `<div class="grey-card">
                   <div class="container">
                       <br>
                           <h4 class="font-dark-blue bold d-flex justify-content-center">`+ province_data[count].key + `</h4>
                       <br>
                       <div class="row">
                           <div class="col-sm">
                               <h5 class="font-yellow bold d-flex justify-content-center">Total Case</h5>
                               <h5 class="font-black bold d-flex justify-content-center">`+ province_data[count].jumlah_kasus + `</h5>
                           </div>
                           <div class="col-sm">
                               <h5 class="font-yellow bold d-flex justify-content-center">On Recovery</h6>
                               <h5 class="font-black bold d-flex justify-content-center">`+ province_data[count].jumlah_dirawat + `</h5>
                           </div>
                           <div class="col-sm">
                               <h5 class="font-yellow bold d-flex justify-content-center">Deaths</h6>
                               <h5 class="font-black bold d-flex justify-content-center">` + province_data[count].jumlah_meninggal + `</h5>
                           </div>
                           <div class="col-sm">
                               <h5 class="font-yellow bold d-flex justify-content-center">Recovered</h6>
                               <h5 class="font-black bold d-flex justify-content-center">`+ province_data[count].jumlah_sembuh + `</h5>
                           </div>
                       </div>
                   </div>
                   <br>
               </div>
               <br>`;
                $('#province').append(html);
                count++;
                $("#search_div").css("visibility", "visible");
            }
        }
    });
    $('#search').keyup(function (e) {
        var search = $("#search").val();
        console.log(search);
        $('#province').empty();
        if (Boolean(search)) {
            var search_uppercase = search.toUpperCase();
            for (let index = 0; index < province_data.length; index++) {
                if (province_data[index].key === search_uppercase) {
                    var html = "";
                    html +=  `<div class="grey-card">
                    <div class="container">
                        <br>
                            <h4 class="font-dark-blue bold d-flex justify-content-center">`+ province_data[index].key + `</h4>
                        <br>
                        <div class="row">
                            <div class="col-sm">
                                <h5 class="font-yellow bold d-flex justify-content-center">Total Case</h5>
                                <h5 class="font-black bold d-flex justify-content-center">`+ province_data[index].jumlah_kasus + `</h5>
                            </div>
                            <div class="col-sm">
                                <h5 class="font-yellow bold d-flex justify-content-center">On Recovery</h6>
                                <h5 class="font-black bold d-flex justify-content-center">`+ province_data[index].jumlah_dirawat + `</h5>
                            </div>
                            <div class="col-sm">
                                <h5 class="font-yellow bold d-flex justify-content-center">Deaths</h6>
                                <h5 class="font-black bold d-flex justify-content-center">` + province_data[index].jumlah_meninggal + `</h5>
                            </div>
                            <div class="col-sm">
                                <h5 class="font-yellow bold d-flex justify-content-center">Recovered</h6>
                                <h5 class="font-black bold d-flex justify-content-center">`+ province_data[index].jumlah_sembuh + `</h5>
                            </div>
                        </div>
                    </div>
                    <br>
                </div>
                <br>`;
                console.log(html);
                $('#province').append(html);
                }
            }
        } else {
            var count = 0;
            for (let index = 0; index < province_data.length; index++) {
                var html = "";
                html += `<div class="grey-card">
                       <div class="container">
                           <br>
                               <h4 class="font-dark-blue bold d-flex justify-content-center">`+ province_data[count].key + `</h4>
                           <br>
                           <div class="row">
                               <div class="col-sm">
                                   <h5 class="font-yellow bold d-flex justify-content-center">Total Case</h5>
                                   <h5 class="font-black bold d-flex justify-content-center">`+ province_data[count].jumlah_kasus + `</h5>
                               </div>
                               <div class="col-sm">
                                   <h5 class="font-yellow bold d-flex justify-content-center">On Recovery</h6>
                                   <h5 class="font-black bold d-flex justify-content-center">`+ province_data[count].jumlah_dirawat + `</h5>
                               </div>
                               <div class="col-sm">
                                   <h5 class="font-yellow bold d-flex justify-content-center">Deaths</h6>
                                   <h5 class="font-black bold d-flex justify-content-center">` + province_data[count].jumlah_meninggal + `</h5>
                               </div>
                               <div class="col-sm">
                                   <h5 class="font-yellow bold d-flex justify-content-center">Recovered</h6>
                                   <h5 class="font-black bold d-flex justify-content-center">`+ province_data[count].jumlah_sembuh + `</h5>
                               </div>
                           </div>
                       </div>
                       <br>
                   </div>
                   <br>`;
                $('#province').append(html);
                count++;
            }
        }
    });
});





