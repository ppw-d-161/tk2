$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "/statistic/data/",
        success: function (response) {
            var html = "";
            html += `<p class ="font-black-muted"> Source : data.covid19.go.id | Last Updated : ` + response.last_updated + `</p>
            <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <h2 class= "font-grey">Positive Cases:</h2>
                <h2 class= "font-black bold">`+ response.postive_case +`</h2>
                <br><br>
                <h2 class= "font-grey">Deaths:</h3>
                <h2 class= "font-black bold">`+ response.deaths +`</h2>
                <br><br>
                <h2 class= "font-grey">Recovered:</h3>
                <h2 class= "font-black bold">` + response.recovered + `</h2>
                <br>
            </div>
            <div class="right-statistic col-xl-6 col-lg-6 col-md-6 col-sm-12">
                <img class="img-responsive" src="https://files.catbox.moe/41i1g5.png" >
            </div>
        </div>`;
            $("#content").append(html);
        }
    });
});