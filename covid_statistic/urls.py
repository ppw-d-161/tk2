from django.urls import path
from .views import statistic_views, statistic_prov_views, statistic_prov_data,statistic_views_data
#url for app

app_name = 'stats'
urlpatterns = [
    path('statistic_prov/', statistic_prov_views, name='stats_prov'),
    path('statistic_prov_data/', statistic_prov_data, name='stats_prov_data'),
    path('data/', statistic_views_data, name='stats_views_data')
]