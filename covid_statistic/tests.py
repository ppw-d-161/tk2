from django.test import TestCase , Client
from django.urls import resolve
from .views import statistic_views, statistic_prov_views, statistic_views_data, statistic_prov_data

# Create your tests here.

class StatisticTest(TestCase):
    # Checking URLs
    def test_statistic_url(self):
        response = Client().get('/')
        self.assertEqual(response.status_code,200)

    def test_statistic_prov_url(self):
        response = Client().get('/statistic/statistic_prov/')
        self.assertEqual(response.status_code,200)

    def test_statistic_prov_data_url(self):
        response = Client().get('/statistic/statistic_prov_data/')
        self.assertEqual(response.status_code,200)

    def test_statistic_data_url(self):
        response = Client().get('/statistic/data/')
        self.assertEqual(response.status_code,200)

    # Checking Views
    def test_statistic_views(self):
        found = resolve('/')
        self.assertEqual(found.func, statistic_views)

    def test_statistic_prov_views(self):
        found = resolve('/statistic/statistic_prov/')
        self.assertEqual(found.func, statistic_prov_views)

    def test_statistic_prov_data_url(self):
        found = resolve('/statistic/statistic_prov_data/')
        self.assertEqual(found.func,statistic_prov_data)

    def test_statistic_data_url(self):
        found =  resolve('/statistic/data/')
        self.assertEqual(found.func,statistic_views_data)

    # Checking Templates
    def test_statistic_templates(self):
        response = Client().get('/')
        self.assertTemplateUsed(response,'statistic.html')

    def test_statistic_prov_templates(self):
        response = Client().get('/statistic/statistic_prov/')
        self.assertTemplateUsed(response,'statistic_prov.html')