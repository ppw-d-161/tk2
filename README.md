[![pipeline status](https://gitlab.com/ppw-d-161/tk1/badges/master/pipeline.svg)](https://gitlab.com/ppw-d-161/tk1/commits/master)


**Kelompok PPW D-14:**
- Gabriel Erwhian Winarso           1906399852 
- Nabila Azzahra                    1906399316
- Martua Reynard William Pasaribu   1906400210
- Faris Sayidinarechan Ardhafa      1906400293

**Link:**
- Herokuapp : https://covinfoppw2.herokuapp.com/
- Wireframe : https://www.figma.com/file/ioFtydpODmfazMkkRVq3l5/Prototype?node-id=0%3A1
- Prototype : https://www.figma.com/file/ioFtydpODmfazMkkRVq3l5/Prototype?node-id=1%3A2
- Persona   : https://www.figma.com/file/iROBvMEvKBVbys76aBsTxU/Persona?node-id=1%3A2

**Tujuan**: 
kami membuat website ini untuk memberikan informasi atau data terupdate untuk covid-19 yang ada di indonesia sehingga masyarakat Indonesia dapat mendapatkan gambaran terhadap perkembangan covid-19 yang ada di Indonesia

**Fitur**:
- Homepage yang berisi statistik covid di indonesia
- About Page
- Statistik per daerah
- Tips menjaga kesehatan
- Fact Checker
- Pesan & Harapan
- Feedback
- Pendaftaran Volunteer

