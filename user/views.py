from django.shortcuts import render
from django.contrib import messages
from django.shortcuts import render, redirect
from .forms import RegisterForm


# Create your views here.
def register_view(request):
    if request.method == "POST":
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            messages.success(request, f"Account Sucessfully Created for {username}. Please Login.")
            return redirect('user:login')
    else:
        form = RegisterForm()
    context = {'form':form}
    return render(request,'register.html',context)