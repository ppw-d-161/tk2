from django.contrib.auth.models import User
from django.test import TestCase , Client
from django.urls import resolve
from .views import register_view

# Create your tests here.
class UserTest(TestCase):

    # Checking URLS
    def test_login_url_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code,200)

    def test_logout_url_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code,200)

    def test_register_url_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code,200)

    # Checking Views
    def test_register_views_exist(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register_view)

    # Checking Templates
    def test_login_templates_exist(self):
        response = Client().get('/login/')
        self.assertTemplateUsed(response, 'login.html')

    def test_logout_templates_exist(self):
        response = Client().get('/logout/')
        self.assertTemplateUsed(response, 'logout.html')

    def test_register_activities_templates_exist(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'register.html')

    # Checking register,login
    def test_register(self):
        response = self.client.post('/register/', {'first_name': 'first', 'last_name': 'last', 'email' : 'asd@gmail.com','username':'hehehe', 'password1' : 'agfeoqwegnhoqwgbqn', 'password2' : 'agfeoqwegnhoqwgbqn'},format='text/html')
        amount = User.objects.count()
        self.assertEqual(amount,1)