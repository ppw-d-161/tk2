import json
from django.forms.models import model_to_dict
from django.http.response import HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib import messages
from .models import FactCheck
from .forms import FactCheckForm
from django.contrib.auth.decorators import login_required
from django.urls import reverse_lazy
from django.core import serializers


# Create your views here.
def info_all(request):
    return render(request, 'info_all.html')

def info_all_data(request):
    unfiltered_fact_check = FactCheck.objects.all()
    confirmed = []
    unconfirmed = []
    for i in unfiltered_fact_check:
        temp = dict()
        temp['id'] = i.id
        temp['title'] = i.title
        temp['user_info'] = i.user_info

        if i.is_checked:
            confirmed.append(temp)
        else:
            unconfirmed.append(temp)

    context = {
                'confirmed': confirmed,
                'unconfirmed' : unconfirmed,
    }

    return JsonResponse(context)

def info_detail(request,id):
    return render(request, 'info_detail.html')

def info_detail_data(request,id):
    obj = FactCheck.objects.get(id=id)

    context = dict()
    context["username"] = obj.user.username
    context["title"] = obj.title
    context["user_info"] = obj.user_info
    context["clarification"] = obj.clarification

    return JsonResponse(context)

@login_required(login_url=reverse_lazy('user:login'))
def info_create(request):
    form = FactCheckForm(request.POST or None)
    context = {'form':form}
    return render(request, 'info_create.html', context)

def info_create_data(request):
    if request.method == 'POST':
        title = request.POST.get('title')
        user_info = request.POST.get('info')

        response_data = dict()

        post = FactCheck(user=request.user,title=title, user_info = user_info)
        post.save()

        response_data['result'] = 'Create post successful!'

        return JsonResponse(response_data)