from django.test import TestCase , Client
from django.urls import resolve
from .models import FactCheck
from .views import info_all , info_detail, info_create, info_all_data, info_detail_data, info_create_data
from django.contrib.auth.models import User

# Create your tests here.

class FactCheckTest(TestCase):
   # Database check
    def setUp(self):
        self.new_user = User.objects.create(username="test",email="hello@gmail.com",password='GWPJE0PWAJWG0WGJ')
        self.fact = FactCheck.objects.create(user=self.new_user ,title='Hoax', user_info = "there is a hoax")

    def test_check_amount_databases(self):
        self.assertEqual(len(FactCheck.objects.all()), 1)

    def test_check_tostring(self):
        obj = FactCheck.objects.get(pk=1)
        self.assertEqual(str(obj), "Hoax")

    # Checking URLs
    def test_info_all_url(self):
        response = Client().get('/Fact-Checker/')
        self.assertEqual(response.status_code,200)

    def test_info_detail_url(self):
        response = Client().get('/Fact-Checker/detail/1/')
        self.assertEqual(response.status_code,200)

    def test_anonymous_create_url(self):
        response = self.client.get('/Fact-Checker/create/')
        self.assertRedirects(response, "/login/?next=/Fact-Checker/create/")

    def test_info_create_url(self):
        self.client.force_login(user=self.new_user)
        response = self.client.get('/Fact-Checker/create/')
        self.assertEqual(response.status_code,200)

    def test_detail_data_url(self):
        response = self.client.get('/Fact-Checker/detail/1/data/')
        self.assertEqual(response.status_code,200)

    def test_info_all_data_url(self):
        response = self.client.get('/Fact-Checker/info_all_data/')
        self.assertEqual(response.status_code,200)

    def test_create_post_url(self):
        self.client.force_login(user=self.new_user)
        response = self.client.post('/Fact-Checker/create_post/',
        {
            'title':'test',
            'info':'info',
        })
        self.assertEqual(response.status_code,200)

    # Checking Views
    def test_info_all_views(self):
        found = resolve('/Fact-Checker/')
        self.assertEqual(found.func, info_all)

    def test_info_detail_views(self):
        found = resolve('/Fact-Checker/detail/1/')
        self.assertEqual(found.func, info_detail)

    def test_info_create_views(self):
        found = resolve('/Fact-Checker/create/')
        self.assertEqual(found.func, info_create)

    def test_detail_data_views(self):
        found = resolve('/Fact-Checker/detail/1/data/')
        self.assertEqual(found.func,info_detail_data)

    def test_info_all_data_views(self):
        found = resolve('/Fact-Checker/info_all_data/')
        self.assertEqual(found.func,info_all_data)

    def test_create_post_views(self):
        found = resolve('/Fact-Checker/create_post/')
        self.assertEqual(found.func,info_create_data)

    # Checking Templates
    def test_info_all_templates(self):
        response = Client().get('/Fact-Checker/')
        self.assertTemplateUsed(response,'info_all.html')

    def test_info_detail_templates(self):
        response = Client().get('/Fact-Checker/detail/1/')
        self.assertTemplateUsed(response,'info_detail.html')

    def test_info_create_templates(self):
        self.client.force_login(user=self.new_user)
        response = self.client.get('/Fact-Checker/create/')
        self.assertTemplateUsed(response,'info_create.html')

    # Checking logic
    '''
    def test_if_HTML_display_models(self):
        response = Client().get('/Fact-Checker/')
        html_response = response.content.decode('utf8')
        self.assertIn("Hoax",html_response)
        self.assertIn("there is a hoax" , html_response)

    # Checking Forms
    def test_saving_a_POST_FactCheckerForm(self):
        self.client.force_login(user=self.new_user)
        response = self.client.post('/Fact-Checker/create/', data={'title' :'Hoax2', 'user_info' : "there is a hoax"})
        amount = FactCheck.objects.filter(title="Hoax2").count()
        self.assertEqual(amount, 1)
    '''