$.ajax({
    type: "GET",
    url: "/Fact-Checker/info_all_data/",
    success: function (response) {
        var html_confirmed ="";
        if (response.confirmed.length != 0) {
            for (let index = 0; index < response.confirmed.length; index++) {
                html_confirmed += `<div>
                <p class="font-yellow bold"><a href="detail/`+ response.confirmed[index].id.toString() + `">`+ response.confirmed[index].title +`</a></p>
                <p class="font-black">`;
                if (response.confirmed[index].user_info.length >= 395) {
                    html_confirmed += response.confirmed[index].user_info.substring(0,395) + "...";
                } else {
                    html_confirmed += response.confirmed[index].user_info;
                }
                html_confirmed += `</p>
                </div>
                <br>`;
            }
        } else {
            html_confirmed += `<br>
            <br>
            <p class="font-black-muted d-flex justify-content-center">Nothing to display here</p>
            <br>
            <br>`;
        }
        $("#confirmed").empty();
        $("#confirmed").append(html_confirmed);
        var html_unconfirmed ="";
        if (response.unconfirmed.length != 0) {
            for (let index = 0; index < response.unconfirmed.length; index++) {
                html_unconfirmed += `<div>
                <p class="font-yellow bold"><a href="detail/`+ response.unconfirmed[index].id.toString() + `">`+ response.unconfirmed[index].title +`</a></p>
                <p class="font-black">`;
                if (response.unconfirmed[index].user_info.length >= 395) {
                    html_unconfirmed += response.unconfirmed[index].user_info.substring(0,395) + "...";
                } else {
                    html_unconfirmed += response.unconfirmed[index].user_info;
                }
                html_unconfirmed += `</p>
                </div>
                <br>`;
            }
        } else {
            html_unconfirmed += `<br>
            <br>
            <p class="font-black-muted d-flex justify-content-center">Nothing to display here</p>
            <br>
            <br>`;
        }
        $("#unconfirmed").empty();
        $("#unconfirmed").append(html_unconfirmed);
    }
});