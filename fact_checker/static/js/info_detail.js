const url = window.location.pathname;
const url_splitted = url.split("/");

$.ajax({
    type: "GET",
    url: "/Fact-Checker/detail/"+ url_splitted[3].toString() + "/data",
    success: function (response) {
        var html = "";
        html += `<h1 class= "font-dark-blue bold">` +  response.title +`</h1>
        <p class="font-black-muted"> Submitted by : ` + response.username +`</p>
        <div class="grey-card">
            <div class="container">
            <br>
                <p>`+ response.user_info +`</p>
            <br>
            </div>
        </div>
        <br>`
        $("#content").append(html);
        var clarification = "";
        if (Boolean(response.clarification)) {
            clarification += `<br>
            <p>`+ response.clarification +`</p>
            <br>`;
        } else {
            clarification += `<br>
            <br>
                <p class="font-black-muted d-flex justify-content-center">Nothing to display here</p>
            <br>
            <br>`
        }
        $("#clarification").empty();
        $("#clarification").append(clarification);
    }
});