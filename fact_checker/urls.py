from django.urls import path
from .views import info_all, info_detail, info_create, info_all_data,info_detail_data,info_create_data
#url for app
app_name = 'fact_checker'

urlpatterns = [
    path('', info_all, name='info-all'),
    path('info_all_data/',info_all_data, name='info-all-data'),
    path('detail/<int:id>/', info_detail, name='info-detail'),
    path('detail/<int:id>/data/', info_detail_data, name='info-detail-data'),
    path('create/', info_create, name='info-create'),
    path('create_post/',info_create_data, name='info-create-post' )
]