from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class FactCheck(models.Model):
    # User Related Info
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)

    # User given info
    title = models.CharField(max_length=100)
    user_info = models.TextField()

    # Admin only
    is_checked = models.BooleanField(default=False)
    clarification = models.TextField()

    def __str__(self):
        return self.title

