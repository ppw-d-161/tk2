from django.contrib import admin
from django.conf.urls import url
from django.urls import include, path
from . import views

app_name = 'info'

urlpatterns = [
    path('', views.info, name='info'),
    path('all_volunteer/', views.volunteer_list, name='all_volunteer'),
    path('buat/', views.buat, name='buat'),
    path('bisa/', views.bisa, name='bisa'),
    path('about/', views.about, name='About'),
]