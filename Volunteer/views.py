from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import inlineformset_factory, modelformset_factory
from django.http.response import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from .forms import VolunteerForm
from .models import Volunteer
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.core.serializers import serialize 

# Create your views here.
def about(request):
    return render(request, 'About.html')

def info(request):
    form = VolunteerForm()
    context = {
        'form': form,
    }
    return render(request, 'info.html', context)

def buat(request):
    if request.method == 'POST':
        volunteer = request.POST['volunteer']
        response_data = dict()
        post = Volunteer(user=request.user, volunteer=volunteer)
        post.save()
        return redirect('info:info')
    

def volunteer_list(request):
    volunteer = Volunteer.objects.all()
    result = []
    for i in volunteer:
        user = User.objects.get(id=i.user.id)
        temp = dict()
        temp['volunteer'] = i.volunteer
        temp['username'] = user.username
        result.append(temp)
    context = {
        'result': result,
    }
    return JsonResponse(context)

@login_required(login_url=('user:login'))
def bisa(request):
    if request.method == "POST":
        form = VolunteerForm(request.POST)
        context = {
            'form': form,
            'volunteer': request.POST['volunteer'],
        }
        return render(request, 'bisa.html', context)
    return redirect('info:info')
