from django import forms

#import model dari models.py
from .models import Volunteer

class VolunteerForm(forms.ModelForm):
    class Meta:
        model = Volunteer
        fields = [
            'volunteer',
        ]

        labels = {
            'volunteer' : 'Tujuan/Kontak',
        }

        widgets = {

            'volunteer' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'id'    : 'volunteer',
                }
            ),
        }