from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve, reverse
from django.contrib.auth.models import User
from .models import MessageAndExpectation
from .forms import MessageAndExpectationForm
from .views import message, confirmation, prevention


class ViewsTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.new_user = User.objects.create(username="test", email="hello@gmail.com", password='GWPJE0PWAJWG0WGJ')
        self.message = MessageAndExpectation.objects.create(user=self.new_user, message='I wish everyone’s life goes back to normal next year')
        self.index = reverse("message:index")
        self.confirmation = reverse("message:confirmation")
        self.prevention = reverse("message:prevention")

    def test_GET_index(self):
        response = self.client.get(self.index)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "message.html")

    def test_GET_confirmation(self):
        response = self.client.get(self.confirmation, follow=True)
        self.assertEqual(response.status_code, 200)

    def test_GET_prevention(self):
        response = self.client.get(self.prevention, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "prevention.html")

    def test_GET_anonymous_confirmation_url(self):
        response = self.client.get('/message/confirmation/')
        self.assertRedirects(response, "/login/?next=/message/confirmation/")

    def test_GET_user_confirmation_url(self):
        self.client.force_login(user=self.new_user)
        response = self.client.get('/message/confirmation/')
        self.assertRedirects(response, "/message/")

    def test_GET_message_all_data(self):
        response = self.client.get('/message/message_all_data/')
        self.assertEqual(response.status_code, 200)
