from django.http import Http404
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import inlineformset_factory, modelformset_factory
from django.http.response import HttpResponse, JsonResponse
from django.contrib.auth.decorators import login_required
from app4.forms import MessageAndExpectationForm
from app4.models import MessageAndExpectation
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.core.serializers import serialize 


# Create your views here.
def prevention(request):
    return render(request, 'prevention.html')

def message(request):
    form = MessageAndExpectationForm()
    context = {
        'form': form,
    }
    return render(request, 'message.html', context)

def message_all_data(request):
    message = MessageAndExpectation.objects.all()
    result = []
    for i in message:
        user = User.objects.get(id=i.user.id)
        temp = dict()
        temp['message'] = i.message
        temp['username'] = user.username
        result.append(temp)
    context = {
        'result': result,
    }
    return JsonResponse(context)

def message_create_data(request):
    if request.method == 'POST':
        message = request.POST.get('message')
        response_data = dict()
        post = MessageAndExpectation(user=request.user, message=message)
        post.save()
        return redirect('message:index')

@login_required(login_url=('user:login'))
def confirmation(request):
    if request.method == "POST":
        form = MessageAndExpectationForm(request.POST)
        context = {
            'form': form,
            'message': request.POST['message']
        }
        return render(request, 'confirmation.html', context)
    return redirect('message:index')




