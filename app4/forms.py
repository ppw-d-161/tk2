from django import forms
from app4.models import MessageAndExpectation


class MessageAndExpectationForm(forms.ModelForm):
    class Meta:
        model = MessageAndExpectation
        fields = (
            'message',
        )

        widgets = {
            'message': forms.Textarea(attrs={'placeholder': 'Insert your message here..', 'id': 'message', 'class': 'form-control'}),
        }
