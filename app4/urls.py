from django.contrib import admin
from django.urls import path, include
from . import views

app_name = 'message'
#url for app, add your URL Configuration

urlpatterns = [
    path('', views.message, name='index'),
    path('message_all_data/', views.message_all_data, name='message-all-data'),
    path('message_create/', views.message_create_data, name='message-create-data'),
    path('confirmation/', views.confirmation, name='confirmation'),
    path('prevention/', views.prevention, name='prevention'),
]
