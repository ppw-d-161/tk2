from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class MessageAndExpectation(models.Model):
    
    #user and message
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    message = models.TextField()
    